import React, { Component }  from 'react';
import Square from './Square';

class Board extends Component {
    SQR(i) {
        const Value= this.props.squares[i];
        return <Square
            value={Value}
            onClick={() => this.props.handleClick(i)}
            index={i}
        />
    }

    render() {
        return (
            <div className="board">
                <div>
                    {this.SQR(0)}
                    {this.SQR(1)}
                    {this.SQR(2)}
                </div>
                <div >
                    {this.SQR(3)}
                    {this.SQR(4)}
                    {this.SQR(5)}
                </div>
                <div>
                    {this.SQR(6)}
                    {this.SQR(7)}
                    {this.SQR(8)}
                </div>
            </div>
        );
    }
}

export default Board;
