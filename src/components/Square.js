import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#000000',
      main: '#4f50b5',
      dark: '#012884',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff8961',
      main: '#f45336',
      dark: '#ba100d',
      contrastText: '#000',
    },
  },
});

class Square extends Component {

  render() {
    const style1={


        fontSize: 100,
        borderRadius: '20',
        maxWidth: '200px',
        maxHeight: '200px',
        minWidth: '200px',
        minHeight: '200px'
    }
    return (
    <Button variant="contained" style={style1}
        id={this.props.index}
        color={this.props.value === 'X' ? 'primary' :'default'}
           color={ this.props.value === 'O' ? 'secondary' : 'default'}
        onClick={() => this.props.onClick()}>

        {this.props.value || ''}
    </Button>
    )
  }
}

export default Square;
