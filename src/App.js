import React, { Component } from "react";
import "./App.css";
import Board from "./components/Board";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import ComputerIcon from '@material-ui/icons/Computer';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import AccessibilityIcon from '@material-ui/icons/Accessibility';


class App extends Component {

  constructor(props) {
    super();
    this.state = this.initialState();
  }

  initialState () {
    return {
      whoMoves: 'X',
      squares: Array(9).fill(null),
      gameT: null,
      winner: null

    };
  }

  reset () {
    this.setState(this.initialState())
  }

  selectGameType (num) {
    this.setState({gameT: num})
  }

  componentDidUpdate() {

    if (this.state.gameT === 3 || (this.state.gameT === 1 && this.state.whoMoves === 'O')) {
      this.computerMove(this.state.squares, this.state.whoMoves);
    }
  }

  handleClick(i) {
    const newSquares = this.state.squares;
    const player = this.state.whoMoves;
    const gameT = this.state.gameT;

    if (gameT === 3) {
      alert('بازی در حال انجام است');
      return
    }
    
    if (!gameT) {
      alert('لطفاً نوع بازی خود را انتخاب فرمایید');
      return
    }

    if (this.state.winner) {
      alert('تبریک برنده بازی شما هستی' + this.state.winner);
      return
    }

    if (newSquares[i]) {
      return
    }

    if (player === 'O' && gameT === 1) {
      alert('لطفاً تا نوبت خود صبر بفرمایید');
      return;
    }

    newSquares[i] = player;
    const winner = this.calculateWinner(newSquares);
    const nextPlay = player === 'X'? 'O' : 'X';

    this.setState({
        whoMoves: nextPlay,
        squares: newSquares,
        winner: winner
    })
  }

  computerMove(squares, player) {
    let newSquares = squares;
    const isEmtpySpot = newSquares.some(el => el === null);


    if (this.state.winner || !isEmtpySpot) {
      return
    }

    let emptySquares = [];
    newSquares.forEach((el, index) => el === null ? emptySquares.push(index) : null)

    setTimeout(() => {
      const randomIndex = Math.floor(Math.random() * emptySquares.length);
      newSquares[emptySquares[randomIndex]] = player;

      const winner = this.calculateWinner(newSquares);
      const nextPlay = player === 'X'? 'O' : 'X';

      this.setState({
        squares: newSquares,
        whoMoves: nextPlay,
        winner: winner
      })
    }, 2000);
}

  calculateWinner(squares) {
    const lines =
      [[0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]];
      for (let i = 0; i< lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] &&
            squares[a] === squares[c]) {
              return squares[a];
        }
      }
      return null;
  }

  render() {
    const winner = this.calculateWinner(this.state.squares);
    const isEmtpySpot = this.state.squares.some(el => el === null);

    let status;
    if (winner) {
      status = 'تبریک برنده بازی شما هستی  ' + winner;
    } else if (!isEmtpySpot) {
        status = 'It\'s a draw!';
      } else {
        status = 'بازیکن بعدی  ' + this.state.whoMoves;
    }
    const gameTText = this.state.gameT === 1 ?
      'بازیکن با کامپیوتر  ' :
      this.state.gameT === 2? 'بازی دو نفره  ' :
      this.state.gameT === 3? 'کامپیوتر با کامپیوتر  ' : null;

    if (!gameTText) {
      
      status = 'لطفاً نوع بازی خود را مشخص فرمایید'

    }
    return (
      <div className="animated.flip" id="App">
        <AppBar position="static">
          <Toolbar>
            <Typography variant='h3' align='center' style={{flexGrow: 1}}>
              <Box textAlign="center">تیک - تاک </Box>
            </Typography>
          </Toolbar>
        </AppBar>

        {!this.state.gameT &&
          <div className="GameT">

          <ButtonGroup className="animated rotateIn" size="large" color="secondary" aria-label="outlined secondary button group">
          <Button variant="outlined" color='primary'
              onClick= {() => this.selectGameType(2)}
              startIcon={<AccessibilityIcon/>}
              endIcon={<AccessibilityIcon/>}> با </Button>
          <Button variant="outlined" color='primary'
              onClick= {() => this.selectGameType(1)}
              startIcon={<AccessibilityIcon/>}
              endIcon={<ComputerIcon />}> با </Button>
          <Button variant="outlined" color='primary'
              onClick= {() => this.selectGameType(3)}
              startIcon={<ComputerIcon/>}
              endIcon={<ComputerIcon />}> با </Button>
          </ButtonGroup>


          </div>
        }

        <Typography variant='h4'>
          {gameTText &&
            <Box className='game-info'  textAlign='center'> نوع بازی : {gameTText}</Box>}
          <Box className='game-info' textAlign="center">{status}</Box>
        </Typography>

        <div className='game'>
          <Board
            squares={this.state.squares}
            handleClick={(i)=> this.handleClick(i)}
          />
          <Button variant="contained" color="secondary"  size="large" font-size="30px" className="reset" onClick= {() => this.reset()}> تلاش دوباره </Button>
        </div>

      </div>
    );
  }
}

export default App;
